#!/bin/sh

GPIO_DIR=/sys/class/gpio


# export test gpio pins
# arg1 - pin number
gpio_export() {
	echo Exporting GPIO $1
	echo $1 > $GPIO_DIR/export
}

# set gpio as output
# arg1 - pin number
gpio_output() {
	echo Set GPIO $1 as output
	echo out > $GPIO_DIR/gpio${1}/direction
}

# set gpio as input
# arg1 - pin number
gpio_input() {
	echo Set GPIO $1 as input
	echo in > $GPIO_DIR/gpio${1}/direction
}

# set gpio in high state
# arg1 - pin number
gpio_set() {
	echo Set GPIO $1
	echo 1 > $GPIO_DIR/gpio${1}/value
}

# set gpio in low state
# arg1 - pin number
gpio_clr() {
	echo Clear GPIO $1
	echo 0 > $GPIO_DIR/gpio${1}/value
}

# read GPIO state
# arg1 - pin number
gpio_state() {
	echo -n "Gpio value = "
	cat $GPIO_DIR/gpio${1}/value
}


# MAIN

echo
echo export several gpio pins
echo

gpio_export 5
gpio_export 6
gpio_export 7
gpio_export 8

echo
echo set them as output
echo

gpio_output 5
gpio_output 6
gpio_output 7
gpio_output 8

echo
echo set high state on pin 5 and 6, low on 7 and 8
echo

gpio_set 5
gpio_set 6
gpio_clr 7
gpio_clr 8

echo
echo read state
echo

gpio_state 5
gpio_state 6
gpio_state 7
gpio_state 8

echo
echo set as input
echo

gpio_input 5
gpio_input 6
gpio_input 7
gpio_input 8

echo
echo read state
echo

gpio_state 5
gpio_state 6
gpio_state 7
gpio_state 8
