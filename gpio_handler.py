#!/usr/bin/python

import RPi.GPIO as GPIO ## Import GPIO Library
import time ## Import 'time' library.  Allows us to use 'sleep'

def Blink(numTimes, gpio):
	for i in range(0,numTimes):
		print "Blink nr " + str(i+1)
		GPIO.output(gpio, True)
		time.sleep(1)
		GPIO.output(gpio, False)
		time.sleep(1)

print "Set mode"
GPIO.setmode(GPIO.BOARD) ## Use BOARD pin numbering
GPIO.setup(7, GPIO.OUT) ## Setup GPIO pin 7 to OUT

print "Start blinking"
Blink(5,7)

print "Done"
GPIO.cleanup()
