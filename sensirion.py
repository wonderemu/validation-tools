# -*- coding: utf-8 -*-

from collections import OrderedDict
import glob
import threading
import time
from os import path
import fcntl
import time
import unittest


class SHT21:
    """Class to read temperature and humidity from SHT21, much of class was 
    derived from:
    http://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/Humidity/Sensirion_Humidity_SHT21_Datasheet_V3.pdf
    and Martin Steppuhn's code from http://www.emsystech.de/raspi-sht21"""

    # control constants
    _SOFTRESET = 0xFE
    _I2C_ADDRESS = 0x40
    _TRIGGER_TEMPERATURE_NO_HOLD = 0xF3
    _TRIGGER_HUMIDITY_NO_HOLD = 0xF5
    _STATUS_BITS_MASK = 0xFFFC

    # From: /linux/i2c-dev.h
    I2C_SLAVE = 0x0703
    I2C_SLAVE_FORCE = 0x0706

    # datasheet (v4), page 9, table 7, thanks to Martin Milata
    # for suggesting the use of these better values
    # code copied from https://github.com/mmilata/growd
    _TEMPERATURE_WAIT_TIME = 0.086  # (datasheet: typ=66, max=85)
    _HUMIDITY_WAIT_TIME = 0.030     # (datasheet: typ=22, max=29)

    def __init__(self, device_number=1):
        """Opens the i2c device (assuming that the kernel modules have been
        loaded).  Note that this has only been tested on first revision
        raspberry pi where the device_number = 0, but it should work
        where device_number=1"""
        self.i2c = open('/dev/i2c-%s' % device_number, 'r+', 0)
        fcntl.ioctl(self.i2c, self.I2C_SLAVE, 0x40)
        self.i2c.write(chr(self._SOFTRESET))
        time.sleep(0.100)

    def read_temperature(self):    
        """Reads the temperature from the sensor.  Not that this call blocks
        for ~86ms to allow the sensor to return the data"""
        self.i2c.write(chr(self._TRIGGER_TEMPERATURE_NO_HOLD))
        time.sleep(self._TEMPERATURE_WAIT_TIME)
        data = self.i2c.read(3)
        if self._calculate_checksum(data, 2) == ord(data[2]):
            return self._get_temperature_from_buffer(data)

    def read_humidity(self):    
        """Reads the humidity from the sensor.  Not that this call blocks 
        for ~30ms to allow the sensor to return the data"""
        self.i2c.write(chr(self._TRIGGER_HUMIDITY_NO_HOLD))
        time.sleep(self._HUMIDITY_WAIT_TIME)
        data = self.i2c.read(3)
        if self._calculate_checksum(data, 2) == ord(data[2]):
            return self._get_humidity_from_buffer(data)

    def close(self):
        """Closes the i2c connection"""
        self.i2c.close()

    def __enter__(self):
        """used to enable python's with statement support"""
        return self

    def __exit__(self, type, value, traceback):
        """with support"""
        self.close()

    @staticmethod
    def _calculate_checksum(data, number_of_bytes):
        """5.7 CRC Checksum using the polynomial given in the datasheet"""
        # CRC
        POLYNOMIAL = 0x131  # //P(x)=x^8+x^5+x^4+1 = 100110001
        crc = 0
        # calculates 8-Bit checksum with given polynomial
        for byteCtr in range(number_of_bytes):
            crc ^= (ord(data[byteCtr]))
            for bit in range(8, 0, -1):
                if crc & 0x80:
                    crc = (crc << 1) ^ POLYNOMIAL
                else:
                    crc = (crc << 1)
        return crc

    @staticmethod
    def _get_temperature_from_buffer(data):
        """This function reads the first two bytes of data and
        returns the temperature in C by using the following function:
        T = =46.82 + (172.72 * (ST/2^16))
        where ST is the value from the sensor
        """
        unadjusted = (ord(data[0]) << 8) + ord(data[1])
        unadjusted &= SHT21._STATUS_BITS_MASK  # zero the status bits
        unadjusted *= 175.72
        unadjusted /= 1 << 16  # divide by 2^16
        unadjusted -= 46.85
        return unadjusted

    @staticmethod
    def _get_humidity_from_buffer(data):
        """This function reads the first two bytes of data and returns
        the relative humidity in percent by using the following function:
        RH = -6 + (125 * (SRH / 2 ^16))
        where SRH is the value read from the sensor
        """
        unadjusted = (ord(data[0]) << 8) + ord(data[1])
        unadjusted &= SHT21._STATUS_BITS_MASK  # zero the status bits
        unadjusted *= 125.0
        unadjusted /= 1 << 16  # divide by 2^16
        unadjusted -= 6
        return unadjusted


class SHT21Test(unittest.TestCase):
    """simple sanity test.  Run from the command line with 
    python -m unittest sht21 to check they are still good"""

    def test_temperature(self):
        """Unit test to check the checksum method"""
        calc_temp = SHT21._get_temperature_from_buffer([chr(99), chr(172)])
        self.failUnless(abs(calc_temp - 21.5653979492) < 0.1)

    def test_humidity(self):
        """Unit test to check the humidity computation using example
        from the v4 datasheet"""
        calc_temp = SHT21._get_humidity_from_buffer([chr(99), chr(82)])
        self.failUnless(abs(calc_temp - 42.4924) < 0.001)

    def test_checksum(self):
        """Unit test to check the checksum method.  Uses values read"""
        self.failUnless(SHT21._calculate_checksum([chr(99), chr(172)], 2) == 249)
        self.failUnless(SHT21._calculate_checksum([chr(99), chr(160)], 2) == 132)
        
        
        

class Sensor(object):
    """
    Representation of one sensor that can measure one or more value.

    Instances of this class are returned by find_sensors_by_type() and find_sensor_by_type().
    """
    def __init__(self, units):
        self._units = units

    def get_units(self):
        """
        :returns a tuple with the units of the values that are measured by this sensor
        """
        return self._units

    def get_values(self):
        """
        :returns a tuple with the current values measured by this sensor
        """
        raise NotImplementedError

    def is_available(self):
        """
        :returns true if the sensor is plugged in and ready to measure
        """
        try:
            self.get_values()
            return True
        except Exception:
            return False

    def get_number_of_values(self):
        return len(self._units)

    def read_file(self, file_path):
        with open(file_path, 'r') as f:
            value = f.read().strip()
        return value


class SFxxSensor(Sensor):
    UNIT_NODE = 'unit'
    VALUE_NODE = 'measured_value'

    def __init__(self, base_path):
        unit_file = path.join(base_path, self.UNIT_NODE)
        unit = self.read_file(unit_file).strip()
        super(SFxxSensor, self).__init__((unit,))
        self.value_file = path.join(base_path, self.VALUE_NODE)

    def get_values(self):
        return float(self.read_file(self.value_file)),


class SHT3xSensor(Sensor):
    TEMPERATURE_NODE = 'hwmon/hwmon0/temp1_input'
    HUMIDITY_NODE = 'hwmon/hwmon0/humidity1_input'
    TEMPERATURE_UNIT = '°C'
    HUMIDITY_UNIT = '%'

    SCALE_FACTOR = 1000

    def __init__(self, base_path):
        super(SHT3xSensor, self).__init__((self.TEMPERATURE_UNIT, self.HUMIDITY_UNIT))
        self.temperature_file = path.join(base_path, self.TEMPERATURE_NODE)
        self.humidity_file = path.join(base_path, self.HUMIDITY_NODE)

    def get_values(self):
        return float(self.read_file(self.temperature_file)) / self.SCALE_FACTOR, \
               float(self.read_file(self.humidity_file)) / self.SCALE_FACTOR


def find_sensors_by_type(sensor_type):
    """
    Find all sensors of a certain type that are connected to the system.

    :param sensor_type: one of "sfm3000", "sdp600", "sfxx", "sht3x"
    :return: a list of Sensor instances.
    """
    sensor_details = {
        'sfm3000': ('40', '0', SFxxSensor),
        'sdp600': ('40', '1', SFxxSensor),
        'sfxx': ('40', '*', SFxxSensor),
        'sht3x': ('44', '*', SHT3xSensor),
    }
    path_template = '/sys/bus/i2c/devices/{0}-00{1}/'
    if sensor_type not in sensor_details:
        raise ValueError('Unknown sensor type ' + sensor_type)

    address, bus, sensor_class = sensor_details[sensor_type]
    pattern = path_template.format(bus, address)
    paths = glob.glob(pattern)
    loaded_sensors = []
    for p in paths:
        try:
            loaded_sensors.append(sensor_class(p))
        except Exception:
            pass
    return [s for s in loaded_sensors if s.is_available()]


def find_sensor_by_type(sensor_type):
    """
    Find one sensor of a certain type that is connected to the system.

    :param sensor_type: one of "sfm3000", "sdp600", "sfxx", "sht3x"
    :return: an instance of the Sensor class or None, if none was found.
    """
    sensors = find_sensors_by_type(sensor_type)
    if sensors:
        return sensors[0]


class SensorReader(threading.Thread):
    """
    Subclass of Thread that periodically reads sensor values and calls a user defined callback.

    The callback is called on the same thread as the measurements are done. Therefore it should return quickly, so as
    to not delay the next measurement. Should the reader not be able to fulfil the required sampling rate, it will
    skip readings, so that the time between measurements will always be a multiple of the measurement period.

    To read sensor values synchronously, call the run() method. This will block the calling thread and call the
    measurement callbacks on the same thread. To cancel, raise an exception from your callback.

    To read sensor values asynchronously, call the start() method. This will start a new thread to read the sensors
    and call the callback. To stop the thread, either raise an exception from the callback or call the stop() method.
    """
    def __init__(self, sensors, frequency, callback):
        """
        Initializes the sensor reader.

        :param sensors: a list of Sensor instances. These will be read out by this instance.
        :param frequency: desired read out frequency in Hz.
        :param callback: a callable to be called on new readings.
        The signature of the callback is as follows:
        - timestamp in seconds
        - ordered dictionary with Sensor instances as keys and the measured values as values
        """
        super(SensorReader, self).__init__(name=SensorReader.__name__)
        self._sensors = sensors
        self._interval = 1.0 / frequency
        self._callback = callback
        self._keep_going = True

    def run(self):
        iteration_start = time.time()
        self._keep_going = True
        while self._keep_going:
            values = OrderedDict()
            for sensor in self._sensors:
                try:
                    sensor_values = sensor.get_values()
                except Exception:
                    sensor_values = (None,) * sensor.get_number_of_values()
                values[sensor] = sensor_values
            self._callback(iteration_start, values)

            now = time.time()
            while iteration_start < now:
                iteration_start += self._interval
            time.sleep(iteration_start - now)

    def join(self, timeout=None):
        self.stop()
        super(SensorReader, self).join(timeout)

    def stop(self):
        self._keep_going = False