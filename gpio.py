# External module imports
import RPi.GPIO as GPIO
import time
import dweepy
print("Start")

# Pin Definitons:
pwmPin = 18 # Broadcom pin 18 (P1 pin 12)
ledPin = 23 # Broadcom pin 23 (P1 pin 16)
butPin = 22 # Broadcom pin 22 (P1 pin 15)
myDweet = {}  
myDweet['sender'] = 'Akash'  

dc = 95 # duty cycle (0-100) for PWM pin
print("Setting Broadcom Mode")
# Pin Setup:
GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
print("Setting Output")

GPIO.setup(ledPin, GPIO.OUT) # LED pin set as output
time.sleep(1.0)
            
GPIO.setup(pwmPin, GPIO.OUT) # PWM pin set as output
time.sleep(1.0)
print("PWM")

pwm = GPIO.PWM(pwmPin, 50)  # Initialize PWM on pwmPin 100Hz frequency
GPIO.setup(butPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Button pin set as input w/ pull-up

# Initial state for LEDs:
GPIO.output(ledPin, GPIO.LOW)

print("Here we go! Press CTRL+C to exit")
try:
    while 1:
        if GPIO.input(butPin): # button is released
            myDweet['Button'] = True 
            GPIO.output(ledPin, GPIO.LOW)
        else: # button is pressed:
            myDweet['Button'] = False 
            GPIO.output(ledPin, GPIO.HIGH)
            time.sleep(0.75)
            GPIO.output(ledPin, GPIO.LOW)
            time.sleep(0.25)
        time.sleep(1) 
        dweepy.dweet_for('akash', myDweet );  
except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
    GPIO.cleanup() # cleanup all GPIO
